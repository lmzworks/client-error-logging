package works.lmz.logging.server;

/**
 * author: Irina Benediktovich - http://plus.google.com/+IrinaBenediktovich
 */
public interface ClientErrorsLogger{
	public void logClientError(ClientErrorData errorData);
}
